export default () => ({
  isSavingCustomStage: false,
  isCreatingCustomStage: false,
  isEditingCustomStage: false,

  formEvents: [],
  formErrors: null,
  formInitialData: null,
});
